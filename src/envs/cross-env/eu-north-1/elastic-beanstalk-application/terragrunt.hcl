include "root" {
  path = find_in_parent_folders()
}

locals {
}

terraform {
  source = "tfr:///cloudposse/elastic-beanstalk-application/aws?version=0.11.1"
}

inputs = {
  # this is important, since used to deploy artifacts to.
  # ci pipeline in application repo will push via
  # aws elasticbeanstalk create-application-version \
  #   --application-name <beanstalk-app> \
  #   --version-label <a unique label for this version of code> \
  #   --description <description of your changes> \
  #   --source-bundle S3Bucket="<bucket name previously noted",S3Key="<key name previously noted"
  # for more info, see: https://stackoverflow.com/questions/51322733/how-to-upload-and-deploy-on-elastic-beanstalk-with-the-aws-cli/51330671#51330671
  name        = "php-app"
  description = "PHP Application"
}