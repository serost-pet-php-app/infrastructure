include "root" {
  path = find_in_parent_folders()
}

terraform {
  source = "tfr:///terraform-aws-modules/iam/aws//modules/iam-user?version=5.33.0"
}

dependency "gitlab_ci_s3_beanstalk_policy" {
  config_path  = "../gitlab-ci-s3-beanstalk-policy"
  mock_outputs = read_terragrunt_config("../gitlab-ci-s3-beanstalk-policy/mocks.hcl").inputs.mocks
}

inputs = {
  create_user                   = true
  create_iam_user_login_profile = false # since used by GitLab CI
  create_iam_access_key         = true

  force_destroy = true

  name = "gitlab-serost-pet-php-app-php-application-root"
  path = "/"

  policy_arns = [
    dependency.gitlab_ci_s3_beanstalk_policy.outputs.arn
  ]
}