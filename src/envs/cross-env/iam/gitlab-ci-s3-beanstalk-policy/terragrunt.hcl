include "root" {
  path = find_in_parent_folders()
}

terraform {
  source = "tfr:///terraform-aws-modules/iam/aws//modules/iam-policy?version=5.33.0"
}

dependency "elastic_beanstalk_application" {
  config_path  = "../../eu-north-1/elastic-beanstalk-application"
  mock_outputs = read_terragrunt_config("../../eu-north-1/elastic-beanstalk-application/mocks.hcl").inputs.mocks
}

inputs = {
  name          = "gitlab-ci-s3-beanstalk-policy"
  create_policy = true
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = [
          "elasticbeanstalk:CreateApplicationVersion",
          "s3:Get*",
          "s3:PutObject"
        ]
        Resource = [
          # Note that '*' is a must, which means "any region" and "any account",
          # whereas ::: (whitespaces) in s3 are used because s3 is a global service
          "arn:aws:elasticbeanstalk:*:*:application/${dependency.elastic_beanstalk_application.outputs.elastic_beanstalk_application_name}",
          "arn:aws:elasticbeanstalk:*:*:applicationversion/${dependency.elastic_beanstalk_application.outputs.elastic_beanstalk_application_name}/*",
          # s3 artifact bucket is managed outside of terragrunt domain.
          "arn:aws:s3:::gitlab-serost-pet-php-app-php-application-root-artifacts/*"
        ]
      }
    ]
  })
}