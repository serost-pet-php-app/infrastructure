inputs = {
  aws_region = "eu-north-1"
  region     = "eu-north-1"

  # will be stored in plaintext within remote state file i believe.
  # therefore, backend needs to be a secure place.
  rds_username = "root"
  rds_password = get_env("TG_DB_PASSWORD")
}