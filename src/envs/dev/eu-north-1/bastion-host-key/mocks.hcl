
inputs = {
  mocks = {
    key_name             = "ssh-key-name"
    private_key          = "ssh-key-private"
    private_key_filename = "ssh-key-private-filename"
    public_key           = "ssh-key-public"
    public_key_filename  = "ssh-key-public-filename"
  }
}