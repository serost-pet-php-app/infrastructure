include "root" {
  path = find_in_parent_folders()
}

terraform {
  source = "tfr:///cloudposse/key-pair/aws?version=0.20.0"
}

dependency "label" {
  config_path  = "../label"
  mock_outputs = read_terragrunt_config("../label/mocks.hcl").inputs.mocks
}

inputs = {
  # private_key and private_key_filename seems empty/invalid when importing
  name                = "bastion-host-key"
  ssh_public_key_path = dirname(get_env("TG_BASTION_PUBLIC_KEY_PATH"))
  ssh_public_key_file = basename(get_env("TG_BASTION_PUBLIC_KEY_PATH"))
  generate_ssh_key    = false

  context = dependency.label.outputs.context
}