
# Things that we get from other terragrunt modules.
variable "elastic_beanstalk_application_name" {}

variable "vpc_id" {}
variable "vpc_default_security_group_id" {}

variable "public_subnet_ids" {
  type = list(string)
}
variable "private_subnet_ids" {
  type = list(string)
}

variable "context" {
  type = any
}

# https://github.com/cloudposse/terraform-aws-elastic-beanstalk-environment/blob/79c464bd3bef617a9c36292fc320900ca1e977b2/examples/complete/main.tf#L38C1-L111C2
module "elb_env_dev_php_app" {
  source = "cloudposse/elastic-beanstalk-environment/aws"

  description                = var.description
  region                     = var.region
  availability_zone_selector = var.availability_zone_selector
  dns_zone_id                = var.dns_zone_id

  wait_for_ready_timeout = var.wait_for_ready_timeout
  # elastic_beanstalk_application_name = module.elb_app_php_app.elastic_beanstalk_application_name
  elastic_beanstalk_application_name = var.elastic_beanstalk_application_name
  environment_type                   = var.environment_type
  loadbalancer_type                  = var.loadbalancer_type
  elb_scheme                         = var.elb_scheme
  tier                               = var.tier
  version_label                      = var.version_label
  force_destroy                      = var.force_destroy

  instance_type    = var.instance_type
  root_volume_size = var.root_volume_size
  root_volume_type = var.root_volume_type

  autoscale_min             = var.autoscale_min
  autoscale_max             = var.autoscale_max
  autoscale_measure_name    = var.autoscale_measure_name
  autoscale_statistic       = var.autoscale_statistic
  autoscale_unit            = var.autoscale_unit
  autoscale_lower_bound     = var.autoscale_lower_bound
  autoscale_lower_increment = var.autoscale_lower_increment
  autoscale_upper_bound     = var.autoscale_upper_bound
  autoscale_upper_increment = var.autoscale_upper_increment

  # https://repost.aws/knowledge-center/elastic-beanstalk-instance-failure
  vpc_id                              = var.vpc_id
  loadbalancer_subnets                = var.public_subnet_ids
  loadbalancer_redirect_http_to_https = false
  # THIS REQUIRES NAT OF SOME SORT TO BE PRESENT IN A NETWORK!!!
  application_subnets = var.private_subnet_ids

  allow_all_egress = true

  additional_security_group_rules = [
    {
      type      = "ingress"
      from_port = 0
      to_port   = 65535
      protocol  = "-1"
      # source_security_group_id = module.vpc.vpc_default_security_group_id
      source_security_group_id = var.vpc_default_security_group_id
      description              = "Allow all inbound traffic from trusted Security Groups"
    }
  ]

  rolling_update_enabled  = var.rolling_update_enabled
  rolling_update_type     = var.rolling_update_type
  updating_min_in_service = var.updating_min_in_service
  updating_max_batch      = var.updating_max_batch

  healthcheck_url  = var.healthcheck_url
  application_port = var.application_port

  # https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html
  # https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html#platforms-supported.docker
  solution_stack_name = var.solution_stack_name

  additional_settings = var.additional_settings
  env_vars            = var.env_vars

  extended_ec2_policy_document = data.aws_iam_policy_document.minimal_s3_permissions.json
  prefer_legacy_ssm_policy     = false
  prefer_legacy_service_policy = false
  scheduled_actions            = var.scheduled_actions

  s3_bucket_versioning_enabled = var.s3_bucket_versioning_enabled
  enable_loadbalancer_logs     = var.enable_loadbalancer_logs

  # context = module.this.context
  context = var.context
}

data "aws_iam_policy_document" "minimal_s3_permissions" {
  statement {
    sid = "AllowS3OperationsOnElasticBeanstalkBuckets"
    actions = [
      "s3:ListAllMyBuckets",
      "s3:GetBucketLocation"
    ]
    resources = ["*"]
  }
}
