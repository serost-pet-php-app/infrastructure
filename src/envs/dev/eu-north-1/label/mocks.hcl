
inputs = {
  mocks = {
    # https://github.com/cloudposse/terraform-null-label/blob/38187a06950aa2a3cfb09a87315c412018315b19/variables.tf
    context = {
      enabled             = true
      namespace           = null
      tenant              = null
      environment         = null
      stage               = "dev"
      name                = null
      delimiter           = "-"
      attributes          = ["public"]
      tags                = {}
      additional_tag_map  = {}
      regex_replace_chars = null
      label_order         = []
      id_length_limit     = null
      label_key_case      = null
      label_value_case    = null
      descriptor_formats  = {}
      # Note: we have to use [] instead of null for unset lists due to
      # https://github.com/hashicorp/terraform/issues/28137
      # which was not fixed until Terraform 1.0.0,
      # but we want the default to be all the labels in `label_order`
      # and we want users to be able to prevent all tag generation
      # by setting `labels_as_tags` to `[]`, so we need
      # a different sentinel to indicate "default"
      labels_as_tags = ["unset"]
    }
  }
}