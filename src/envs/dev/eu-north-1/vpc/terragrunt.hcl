include "root" {
  path = find_in_parent_folders()
}

terraform {
  source = "tfr:///cloudposse/vpc/aws?version=2.1.0"
}

dependency "label" {
  config_path = "../label"

  mock_outputs = read_terragrunt_config("../label/mocks.hcl").inputs.mocks
}

inputs = {
  ipv4_primary_cidr_block = "172.16.0.0/16"

  context = dependency.label.outputs.context
}