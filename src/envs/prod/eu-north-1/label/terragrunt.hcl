include "root" {
  path = find_in_parent_folders()
}

terraform {
  source = "tfr:///cloudposse/label/null/?version=0.25.0"
}

inputs = {
  stage      = "prod"
  attributes = ["public"]
  delimiter  = "-"

  tags = {
  }
}