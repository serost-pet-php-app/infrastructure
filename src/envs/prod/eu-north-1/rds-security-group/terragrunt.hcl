include "root" {
  path = find_in_parent_folders()
}

terraform {
  source = "tfr:///cloudposse/security-group/aws?version=2.2.0"
}

locals {
  regional_env = read_terragrunt_config(find_in_parent_folders("regional.env.hcl"))
}

dependency "label" {
  config_path = "../label"

  mock_outputs = read_terragrunt_config("../label/mocks.hcl").inputs.mocks
}

dependency "vpc" {
  config_path  = "../vpc"
  mock_outputs = read_terragrunt_config("../vpc/mocks.hcl").inputs.mocks
}

inputs = merge(local.regional_env.inputs, {

  vpc_id  = dependency.vpc.outputs.vpc_id
  context = dependency.label.outputs.context

  rules = [
    {
      type        = "ingress"
      from_port   = 3306
      to_port     = 3306
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
      description = "MySQL from anywhere"
    }
  ]
})