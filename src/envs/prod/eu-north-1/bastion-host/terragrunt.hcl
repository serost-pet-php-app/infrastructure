include "root" {
  path = find_in_parent_folders()
}

terraform {
  source = "tfr:///cloudposse/ec2-instance/aws?version=1.2.0"
}

locals {
  regional_env = read_terragrunt_config(find_in_parent_folders("regional.env.hcl"))
}

dependency "label" {
  config_path  = "../label"
  mock_outputs = read_terragrunt_config("../label/mocks.hcl").inputs.mocks
}

dependency "vpc" {
  config_path  = "../vpc"
  mock_outputs = read_terragrunt_config("../vpc/mocks.hcl").inputs.mocks
}

dependency "subnets" {
  config_path  = "../subnets"
  mock_outputs = read_terragrunt_config("../subnets/mocks.hcl").inputs.mocks
}

dependency "bastion_host_key" {
  config_path  = "../bastion-host-key"
  mock_outputs = read_terragrunt_config("../bastion-host-key/mocks.hcl").inputs.mocks
}

inputs = merge(local.regional_env.inputs, {
  vpc_id       = dependency.vpc.outputs.vpc_id
  subnet       = dependency.subnets.outputs.public_subnet_ids[0]
  ssh_key_pair = dependency.bastion_host_key.outputs.key_name

  name    = "bastion"
  context = dependency.label.outputs.context

  assign_eip_address          = false
  associate_public_ip_address = true

  instance_type = "t3.micro"

  # default ami is ubuntu16

  security_group_rules = [
    {
      type        = "egress"
      from_port   = 0
      to_port     = 65535
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      type        = "ingress"
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  ]
})