include "root" {
  path = find_in_parent_folders()
}

locals {
  regional_env = read_terragrunt_config(find_in_parent_folders("regional.env.hcl"))
}

dependency "label" {
  config_path  = "../label"
  mock_outputs = read_terragrunt_config("../label/mocks.hcl").inputs.mocks
}

dependency "vpc" {
  config_path  = "../vpc"
  mock_outputs = read_terragrunt_config("../vpc/mocks.hcl").inputs.mocks
}

dependency "subnets" {
  config_path  = "../subnets"
  mock_outputs = read_terragrunt_config("../subnets/mocks.hcl").inputs.mocks
}

dependency "elastic_beanstalk_application" {
  config_path  = "../../../cross-env/eu-north-1/elastic-beanstalk-application"
  mock_outputs = read_terragrunt_config("../../../cross-env/eu-north-1/elastic-beanstalk-application/mocks.hcl").inputs.mocks
}

dependency "rds" {
  config_path  = "../rds"
  mock_outputs = read_terragrunt_config("../rds/mocks.hcl").inputs.mocks
}

# https://github.com/cloudposse/terraform-aws-elastic-beanstalk-environment/blob/79c464bd3bef617a9c36292fc320900ca1e977b2/examples/complete/fixtures.us-east-2.tfvars
inputs = merge(local.regional_env.inputs, {
  elastic_beanstalk_application_name = dependency.elastic_beanstalk_application.outputs.elastic_beanstalk_application_name

  vpc_id                        = dependency.vpc.outputs.vpc_id
  vpc_default_security_group_id = dependency.vpc.outputs.vpc_default_security_group_id

  public_subnet_ids  = dependency.subnets.outputs.public_subnet_ids
  private_subnet_ids = dependency.subnets.outputs.private_subnet_ids

  context = dependency.label.outputs.context

  availability_zones         = ["eu-north-1a", "eu-north-1b"]
  namespace                  = "eg"
  stage                      = "prod"
  name                       = "eb-prod"
  description                = "PHP Application"
  tier                       = "WebServer"
  environment_type           = "LoadBalanced"
  loadbalancer_type          = "application"
  availability_zone_selector = "Any 2"
  instance_type              = "t3.micro"
  autoscale_min              = 1
  autoscale_max              = 1
  wait_for_ready_timeout     = "10m"
  force_destroy              = true
  rolling_update_enabled     = true
  rolling_update_type        = "Health"
  updating_min_in_service    = 0
  updating_max_batch         = 1
  healthcheck_url            = "/"
  application_port           = 80
  root_volume_size           = 8
  root_volume_type           = "gp2"
  autoscale_measure_name     = "CPUUtilization"
  autoscale_statistic        = "Average"
  autoscale_unit             = "Percent"
  autoscale_lower_bound      = 20
  autoscale_lower_increment  = -1
  autoscale_upper_bound      = 80
  autoscale_upper_increment  = 1
  elb_scheme                 = "public"
  // https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html
  // https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html#platforms-supported.docker
  solution_stack_name = "64bit Amazon Linux 2023 v4.0.4 running PHP 8.2"

  # we get version from application repo!
  # in a cool GitOps environment, the application repo may 
  # commit (or do a merge request) to change the version, but
  # to keep things simple, manual version change will do the trick.
  # Current:
  # https://gitlab.com/serost-pet-php-app/php-application-root/-/tree/c5fe6e75d27ad944dea8c34b0c191f1223e274c7

  # this one is reasonably stable, since from main branch
  version_label = "php-app-artifact-main-b0b2e17b32c8bdb80e1f3544a2938661714d0989"

  dns_zone_id = ""
  // https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/command-options-general.html
  additional_settings = [
    {
      namespace = "aws:elasticbeanstalk:environment:process:default"
      name      = "StickinessEnabled"
      value     = "false"
    },
    {
      namespace = "aws:elasticbeanstalk:managedactions"
      name      = "ManagedActionsEnabled"
      value     = "false"
    }
  ]
  env_vars = {
    "DB_ENDPOINT" = dependency.rds.outputs.instance_endpoint
    "DB_USERNAME" = local.regional_env.inputs.rds_username
    "DB_PASSWORD" = local.regional_env.inputs.rds_password
  }
  s3_bucket_versioning_enabled = false
  enable_loadbalancer_logs     = false
})