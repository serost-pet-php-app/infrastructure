include "root" {
  path = find_in_parent_folders()
}

locals {
  regional_env = read_terragrunt_config(find_in_parent_folders("regional.env.hcl"))
}

terraform {
  source = "tfr:///cloudposse/dynamic-subnets/aws?version=2.4.1"
}

dependency "label" {
  config_path  = "../label"
  mock_outputs = read_terragrunt_config("../label/mocks.hcl").inputs.mocks
}

dependency "vpc" {
  config_path  = "../vpc"
  mock_outputs = read_terragrunt_config("../vpc/mocks.hcl").inputs.mocks
}

# merge will overwrite and expand things from first argument,
# with the second one, see: https://developer.hashicorp.com/terraform/language/functions/merge
inputs = merge(local.regional_env.inputs, {
  nat_gateway_enabled  = false
  nat_instance_enabled = true

  nat_instance_type = "t3.micro"
  max_nats          = 1

  # vpc module outputs strings:
  # https://github.com/cloudposse/terraform-aws-vpc/blob/cd9070d36b6b837e9b5b51fdc2bbfd75b813348c/outputs.tf
  # but subnet module requires (sometimes) lists of strings
  # https://github.com/cloudposse/terraform-aws-dynamic-subnets/blob/236168c0ab15408619126d931a22647ced8286e2/variables.tf

  vpc_id          = dependency.vpc.outputs.vpc_id
  igw_id          = [dependency.vpc.outputs.igw_id]
  ipv4_cidr_block = [dependency.vpc.outputs.vpc_cidr_block]

  context = dependency.label.outputs.context
})