include "root" {
  path = find_in_parent_folders()
}

terraform {
  source = "tfr:///cloudposse/rds/aws?version=1.1.0"
}

locals {
  regional_env = read_terragrunt_config(find_in_parent_folders("regional.env.hcl"))
}

dependency "label" {
  config_path = "../label"

  mock_outputs = read_terragrunt_config("../label/mocks.hcl").inputs.mocks
}

dependency "vpc" {
  config_path  = "../vpc"
  mock_outputs = read_terragrunt_config("../vpc/mocks.hcl").inputs.mocks
}

dependency "subnets" {
  config_path  = "../subnets"
  mock_outputs = read_terragrunt_config("../subnets/mocks.hcl").inputs.mocks
}

dependency "rds_security_group" {
  config_path  = "../rds-security-group"
  mock_outputs = read_terragrunt_config("../rds-security-group/mocks.hcl").inputs.mocks
}

inputs = merge(local.regional_env.inputs, {
  vpc_id = dependency.vpc.outputs.vpc_id

  subnet_ids = dependency.subnets.outputs.private_subnet_ids
  # im not sure what is the difference here btw
  security_group_ids           = [dependency.rds_security_group.outputs.id]
  associate_security_group_ids = [dependency.rds_security_group.outputs.id]
  context                      = dependency.label.outputs.context

  name                = "php-app-db"
  deletion_protection = false
  database_name       = "php_app_db"
  database_user       = local.regional_env.inputs.rds_username
  database_password   = local.regional_env.inputs.rds_password
  database_port       = 3306
  multi_az            = false
  storage_type        = "standard"
  storage_encrypted   = false
  allocated_storage   = 5
  engine              = "mysql"
  engine_version      = "5.7"
  instance_class      = "db.t3.micro"
  db_parameter_group  = "mysql5.7"
  publicly_accessible = false
  apply_immediately   = true
})