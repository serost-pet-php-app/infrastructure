
inputs = {
  mocks = {
    vpc_id                        = "vpc-00000000000000000"
    igw_id                        = "igw-00000000000000000"
    vpc_cidr_block                = "10.0.0.0/16"
    vpc_default_security_group_id = "sg-fake"
  }
}